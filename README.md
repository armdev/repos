docker login registry.gitlab.com

docker build -t registry.gitlab.com/armdev/dockers .

docker push registry.gitlab.com/armdev/dockers


docker build -t registry.gitlab.com/armdev/dockers/postgres15 postgres:15

docker push registry.gitlab.com/armdev/dockers/postgres15

docker build -t registry.gitlab.com/armdev/dockers/postgres16 postgres:16

docker push registry.gitlab.com/armdev/dockers/postgres16

docker build -t registry.gitlab.com/armdev/dockers/timescale timescale

docker push registry.gitlab.com/armdev/dockers/timescale

docker build -t registry.gitlab.com/armdev/dockers/pgadmin:4 pgadmin:4

docker push registry.gitlab.com/armdev/dockers/pgadmin:4

docker build -t registry.gitlab.com/armdev/dockers/mongo4 mongo:4.4

docker push registry.gitlab.com/armdev/dockers/mongo4

docker build -t registry.gitlab.com/armdev/dockers/mongo6 mongo:6

docker push registry.gitlab.com/armdev/dockers/mongo6

docker build -t registry.gitlab.com/armdev/dockers/mongo7 mongo:7

docker push registry.gitlab.com/armdev/dockers/mongo7

docker build -t registry.gitlab.com/armdev/dockers/eclipse-temurin:20 eclipse-temurin:20

docker push registry.gitlab.com/armdev/dockers/eclipse-temurin:20

docker build -t registry.gitlab.com/armdev/dockers/eclipse-temurin:21 eclipse-temurin:21

docker push registry.gitlab.com/armdev/dockers/eclipse-temurin:21

docker build -t registry.gitlab.com/armdev/dockers/zulujdk:21 zulujdk:21

docker push registry.gitlab.com/armdev/dockers/zulujdk:21

docker build -t registry.gitlab.com/armdev/dockers/liberica:21 liberica:21

docker push registry.gitlab.com/armdev/dockers/liberica:21

docker build -t registry.gitlab.com/armdev/dockers/tomee:8.0.15 tomee:8.0.15

docker push registry.gitlab.com/armdev/dockers/tomee:8.0.15

docker build -t registry.gitlab.com/armdev/dockers/plume-java21 plume-java21

docker push registry.gitlab.com/armdev/dockers/plume-java21

docker build -t registry.gitlab.com/armdev/dockers/postgres16.3 postgres:16.3

docker push registry.gitlab.com/armdev/dockers/postgres16.3

docker build -t registry.gitlab.com/armdev/dockers/bitnami_postgres16 bitnami_postgres16

docker push registry.gitlab.com/armdev/dockers/bitnami_postgres16

docker build -t registry.gitlab.com/armdev/dockers/eclipse-temurin:22 eclipse-temurin:22

docker push registry.gitlab.com/armdev/dockers/eclipse-temurin:22

docker build -t registry.gitlab.com/armdev/dockers/timescale16 timescale16

docker push registry.gitlab.com/armdev/dockers/timescale16

docker build -t registry.gitlab.com/armdev/dockers/kafka7 kafka7

docker push registry.gitlab.com/armdev/dockers/kafka7

docker build -t registry.gitlab.com/armdev/dockers/zookeeper:7.6.1 zookeeper:7.6.1

docker push registry.gitlab.com/armdev/dockers/zookeeper:7.6.1

docker build -t registry.gitlab.com/armdev/dockers/minio minio

docker push registry.gitlab.com/armdev/dockers/minio

docker build -t registry.gitlab.com/armdev/dockers/redis:7.2 redis:7.2

docker push registry.gitlab.com/armdev/dockers/redis:7.2

docker build -t registry.gitlab.com/armdev/dockers/cassandra:4.1 cassandra4.1

docker push registry.gitlab.com/armdev/dockers/cassandra:4.1

